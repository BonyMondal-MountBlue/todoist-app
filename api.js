const token = "169c4101e6f15ece4768b4008a531298a9aa6b17";
//Get a project
function GetAProject() {
  return new Promise((resolve, reject) => {
    fetch(" https://api.todoist.com/rest/v1/projects", {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then((data) => data.json())
      .then((data) => {
        resolve(data);
      })
      .catch((err) => reject(err));
  });
}

// Get an active task
function GetActiveTask() {
  return new Promise((resolve, reject) => {
    fetch("https://api.todoist.com/rest/v1/tasks", {
      headers: {
        Authorization: "Bearer 169c4101e6f15ece4768b4008a531298a9aa6b17"
      }
    })
      .then((data) => data.json())
      .then((data) => {
        resolve(data);
      })
      .catch((err) => reject(err));
  });
}

//Create a new task

var myHeaders = new Headers();
myHeaders.append(
  "Authorization",
  "Bearer 169c4101e6f15ece4768b4008a531298a9aa6b17"
);
myHeaders.append("Content-Type", "application/json");

let date = document.getElementById("start");
let inputTask = document.getElementById("inputtask");
let inputDescription = document.getElementById("inputdescription");
let AddTask = document.getElementById("addtask");
function CreateNewTask() {
  return new Promise((resolve, reject) => {
    AddTask.addEventListener("click", () => {
      var requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: JSON.stringify({
          content: `${inputTask.value}`,
          description: `${inputDescription.value}`,
          due_string: `${date.value}`
        }),
        redirect: "follow"
      };
      function CreateTask() {
        return new Promise((resolve, reject) => {
          fetch("https://api.todoist.com/rest/v1/tasks", requestOptions)
            .then((response) => response.json())
            .then((result) => resolve(result))
            .catch((error) => reject(error));
        });
      }
      CreateTask()
        .then((data) => {
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  });
}

// Adding Comment to a task
function AddComment(task_id, new_comment) {
  return new Promise((resolve, reject) => {
    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer 169c4101e6f15ece4768b4008a531298a9aa6b17"
    );
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      task_id: task_id,
      content: `${new_comment}`
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow"
    };

    fetch("https://api.todoist.com/rest/v1/comments", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        resolve(result);
      })
      .catch((error) => reject(error));
  });
}
