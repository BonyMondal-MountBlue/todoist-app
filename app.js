// Toggle menu
function myFunction() {
  var ToggleMenu = document.getElementById("clicked");
  if (ToggleMenu.style.display === "block") {
    ToggleMenu.style.display = "none";
    ToggleMenu.style.tr;
  } else {
    ToggleMenu.style.display = "block";
  }
}

// View today with date
const Today = document.getElementById("clicktoday");
Today.addEventListener("click", (responce) => {
  responce.preventDefault();
  let options = { weekday: "long", month: "short", day: "numeric" };
  let today = new Date();
  let viewToday = document.getElementById("view-upcoming-calender");
  if (viewToday.hasChildNodes) {
    viewToday.innerHTML = "";
  }
  let HeaderDate = document.createElement("h1");
  let showToday = document.createElement("span");
  let date = document.createElement("small");
  showToday.textContent = "Today";
  HeaderDate.appendChild(showToday);
  date.innerHTML = today.toLocaleDateString("en-US", options);
  HeaderDate.appendChild(date);
  viewToday.appendChild(HeaderDate);
});

// Showing active tasks
let addTask = document.getElementById("clicktoday");
addTask.addEventListener("click", (response) => {
  response.preventDefault();
  GetActiveTask()
    .then((data) => {
      console.log(data);
      let divTask = document.getElementById("view_task");
      for (let index = 0; index < data.length; index++) {
        try {
          let div = document.createElement("div");
          let div_Task = document.createElement("div");
          let header = document.createElement("h1");
          let small = document.createElement("small");
          let hr = document.createElement("hr");
          let input = document.createElement("input");
          input.setAttribute("id", "completed");
          input.setAttribute("type", "radio");
          div_Task.setAttribute("onclick", "addSubTask()");
          header.textContent = data[index].content;
          small.textContent = data[index].due.date;
          div.appendChild(input);
          div.appendChild(header);
          div_Task.appendChild(div);
          div_Task.appendChild(small);
          div_Task.appendChild(hr);
          divTask.appendChild(div_Task);
          div.style.display = "flex";
        } catch {
          let div = document.createElement("div");
          let div_Task = document.createElement("div");
          div_Task.setAttribute("onclick", "addSubTask()");
          let header = document.createElement("h1");
          let hr = document.createElement("hr");
          let input = document.createElement("input");
          input.setAttribute("id", "completed");
          input.setAttribute("type", "radio");
          header.textContent = data[index].content;
          div.appendChild(input);
          div.appendChild(header);
          div_Task.appendChild(div);
          div_Task.appendChild(header);
          div_Task.appendChild(hr);
          divTask.appendChild(div_Task);
          div.style.display = "flex";
        }
      }
    })
    .catch((err) => {
      console.log(err);
    });
});

//Showing modal
let TaskButton = document.getElementById("task-button");
let add_task = document.getElementById("add task");
let modal = document.getElementById("modal");
let cancel = document.getElementById("cancel");
TaskButton.addEventListener("click", (responce) => {
  responce.preventDefault();
  modal.style.display = "block";
});
add_task.addEventListener("click", (responce) => {
  responce.preventDefault();
  modal.style.display = "block";
});
cancel.addEventListener("click", (response) => {
  response.preventDefault();
  modal.style.display = "none";
});

// Create task from dashboard
CreateNewTask().then((data) => {
  let divTask = document.getElementById("view_task");
  let div = document.createElement("div");
  let div_Task = document.createElement("div");
  let header = document.createElement("h1");
  let input = document.createElement("input");
  let small = document.createElement("small");
  let duetime = document.createElement("h3");
  let hr = document.createElement("hr");
  input.setAttribute("id", "completed");
  input.setAttribute("type", "radio");
  header.textContent = data.content;
  small.textContent = data.description;
  duetime.textContent = data.due.date;
  div_Task.setAttribute("onclick", "addSubTask()");
  div.appendChild(input);
  div.appendChild(header);
  div_Task.appendChild(div);
  div_Task.appendChild(small);
  div_Task.appendChild(duetime);
  div_Task.appendChild(hr);
  divTask.appendChild(div_Task);
  div.style.display = "flex";
});

//Adding subtask
function addSubTask() {
  let TaskButton = document.getElementById("task-button");
  let modal = document.getElementById("modal");
  let cancel = document.getElementById("cancel");
  TaskButton.addEventListener("click", (responce) => {
    responce.preventDefault();
    modal.style.display = "block";
  });
  cancel.addEventListener("click", (response) => {
    response.preventDefault();
    modal.style.display = "none";
  });
  let divTask = document.getElementById("view_task");

  let divSubTask = document.getElementById("subtask");

  divSubTask.append(divTask.querySelector("h1"));
  divTask.innerHTML = "";
  CreateNewTask().then((data) => {
    let div = document.createElement("div");
    let div_Task = document.createElement("div");
    let header = document.createElement("h1");
    let input = document.createElement("input");
    let small = document.createElement("small");
    let duetime = document.createElement("h3");
    let hr = document.createElement("hr");
    input.setAttribute("id", "completed");
    input.setAttribute("type", "radio");
    header.textContent = data.content;
    small.textContent = data.description;
    duetime.textContent = data.due.date;
    div_Task.setAttribute("onclick", "addSubTask()");
    div.appendChild(input);
    div.appendChild(header);
    div_Task.appendChild(div);
    div_Task.appendChild(small);
    div_Task.appendChild(duetime);
    div_Task.appendChild(hr);
    divSubTask.appendChild(div_Task);
    div.style.display = "flex";
    if (divTask.hasChildNodes) {
      divTask.innerHTML = "";
    }
    let TaskButton = document.getElementById("task-button");
    let modal = document.getElementById("modal");
    let cancel = document.getElementById("cancel");
    TaskButton.addEventListener("click", (responce) => {
      responce.preventDefault();
      modal.style.display = "block";
    });
    cancel.addEventListener("click", (response) => {
      response.preventDefault();
      modal.style.display = "none";
    });
    //Adding comment to a task

    let comment = document.getElementById("inputcomment");
    AddComment(data.id, comment.value).then((data) => {
      let comment = document.createElement("h3");
      let hr = document.createElement("hr");
      comment.textContent = data.content;

      divSubTask.appendChild(comment);
      divSubTask.appendChild(hr);
    });
  });
}
